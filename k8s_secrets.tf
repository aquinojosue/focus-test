resource "digitalocean_container_registry_docker_credentials" "do_registry" {
  registry_name = "focus-test-jaquino"
}

resource "kubernetes_secret" "registry_key" {
  for_each = toset(var.cluster_namespaces)
  metadata {
    name = "docker-cfg"
    namespace = each.key
  }

  data = {
    ".dockerconfigjson" = digitalocean_container_registry_docker_credentials.do_registry.docker_credentials
  }

  type = "kubernetes.io/dockerconfigjson"
  depends_on = [
    digitalocean_container_registry_docker_credentials.do_registry,
    kubernetes_namespace.focus-ns
  ]
}