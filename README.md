# Focus Code Test - Josué David Aquino Barrera
This repo holds a terraform cluster deployment to DigitalOcean using Helm Charts

## Installation/Deployment
First thing to do before applying changes using terraform, you'll have to import your Personal Access Token [(more info)](https://docs.digitalocean.com/reference/api/create-personal-access-token/), once you have generated your token, you'll have to paste it in the `digitalocean_token` variable of the `variables.tf` file.

Then, initialize terraform with the command `terraform init`.

Now you can deploy using the command `terraform apply`.

## Gitlab CI/CD
To use gitlab CI/CD with this repo, you'll have to add your Personal Access Token and the UUID of the deployed cluster as CI/CD variables:

* `DIGITALOCEAN_TOKEN` for the Personal Access Token
* `DO_CLUSTER_ID` for the Cluster ID.

after that, any changes made to the repo will be deployed to the k8s cluster.

## Desired results
### On *production* environment (**prod**.focus-test.local)
![image info](./assets/desired-result-prod.png)

### On *staging* environment (**staging**.focus-test.local)
![image info](./assets/desired-result-staging.png)