resource "digitalocean_kubernetes_cluster" "kubernetes_cluster" {
  name    = var.cluster_name
  region  = var.cluster_region
  version = var.cluster_version

  tags = ["focus", "test"]

  node_pool {
    name       = "default-pool"
    size       = "s-2vcpu-4gb"
    auto_scale = false
    node_count = var.cluster_nodes
    tags       = ["node-pool-tag"]
  }

}

data "digitalocean_kubernetes_cluster" "kubernetes_cluster" {
  name = var.cluster_name
  depends_on       = [digitalocean_kubernetes_cluster.kubernetes_cluster]
}