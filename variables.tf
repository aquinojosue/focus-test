variable "digitalocean_token" {
    default = ""
    type = string
}

variable "cluster_name" {
    default = "focus-test-jaquino"
}

variable "cluster_nodes" {
    default = "4"
}

variable "cluster_region" {
    default = "ams3"
}

variable "cluster_version" {
    default = "1.21.5-do.0"
}

variable "do_container_registry"{
    default = "focus-test-jaquino"
}

variable "cluster_namespaces" {
    type = list(string)
    default = ["focus-test-app-staging", "focus-test-app-production"]
}