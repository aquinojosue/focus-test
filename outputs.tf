output "kubeconfig" {
    value = nonsensitive(data.digitalocean_kubernetes_cluster.kubernetes_cluster.kube_config.0.raw_config)
}