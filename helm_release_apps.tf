resource "helm_release" "focus-test" {
  name        = "focus-test"
  chart       = "focus-test"
  repository  = "./charts"
  namespace   = "focus-test-app-production"
  max_history = 3
  create_namespace = false
  wait             = true
  reset_values     = true
  depends_on = [
    kubernetes_namespace.focus-ns, kubernetes_secret.registry_key
  ]
}

resource "helm_release" "focus-test-staging" {
  name        = "focus-test"
  chart       = "focus-test"
  repository  = "./charts"
  namespace   = "focus-test-app-staging"
  values = [
    "${file("values-staging.yaml")}"
  ]
  max_history = 3
  create_namespace = false
  wait             = true
  reset_values     = true
  depends_on = [
    kubernetes_namespace.focus-ns, kubernetes_secret.registry_key
  ]
}
