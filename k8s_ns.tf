resource "kubernetes_namespace" "focus-ns"{
  for_each = toset(var.cluster_namespaces)
  metadata {
    annotations = {
      name = each.key
    }
    

    labels = {
      istio-injection = "enabled"
    }

    name = each.key
  }
  depends_on = [digitalocean_kubernetes_cluster.kubernetes_cluster, kubernetes_namespace.istio_system, helm_release.istiod]
}